// TODO: Rename postnr, vejnavn, etc. to from Danish to the corporate language: Bad English.
// TODO: Docblock functions

var config = {};
var caching = {}; // Holds data from the server

function setup(settings) {
  $.extend(config, settings);
}

function compare(query, match) {
  return match.toLowerCase().indexOf(query.toLowerCase()) === 0;
}

function addToCollection(collection, key, value) {
  if (!collection[key]) {
    collection[key] = [];
  }
  collection[key].push(value);
}

/**
 * [matchStreets description]
 * @param  {[type]} data  [description]
 * @param  {[type]} query [description]
 * @return {[type]}       [description]
 */
function matchStreets(data, query) {
  var matches = {};
  if (data.husnumre) {
    for (var i = 0; i < data.husnumre.length; i++) {

      var street = data.husnumre[i].vejnavn;
      if (compare(query, street)) {
        addToCollection(matches, street, data.husnumre[i]);
      }

    }
  }

  return matches;
}

function matchHouses(streetName, data, query) {
  var matches = {};
  if (data[streetName]) {

    for (var i = 0; i < data[streetName].length; i++) {
      var house = streetName + ' ' + data[streetName][i].husnr;

      if (compare(query, house)) {
        addToCollection(matches, house, data[streetName][i]);
      }

    }
  }

  return matches;
}

function matchFloors(streetName, houseNumber, data, query) {
  var matches = {};
  for (var i = 0; i < data.length; i++) {
    // TODO: Lav det her mere robust. Det virker ikke, hvis f.eks. data[i] = {etage: 0, ...}
    var address = data[i].vejnavn + ' ' + data[i].husnr
      + (data[i].etage ? ' ' + data[i].etage : '')
      + (data[i].doernr ? ' ' + data[i].doernr : '');

    if (compare(query, address)) {
      addToCollection(matches, address, data[i]);
    }
  }

  return matches;
}

// Loads housenumber data and finds the streetname(s) that match input query.
function loadStreets(postnr, query, promise) {
  // TODO: Should loadData handle substr of streetname?
  loadData(postnr, query.substr(0, 1)).done(function(data) {
    var result = [];
    var returnresult = true;
    var q = '';

    for (var i = 0, length = query.length; i < length; i++) {
      q += query[i];
      var matches = matchStreets(data, q);
      result = Object.keys(matches);
      if (result.length === 1) {
        // TODO: Optimise when we have found a single match
        if (result[0].length <= q.length) {
          loadHouses(postnr, result[0], query, matches, promise);
          returnresult = false;
        }
      }
    }

    if (returnresult) {
      resolveResult(promise, matches);
    }
  }).fail(function(error) {
    promise.reject(error);
  });
}

// Finds any housenumber(s) on the street that match input query.
function loadHouses(postnr, vejnavn, query, previousmatches, promise) {
  var result = [];
  var returnresult = true;

  // TODO: Det har skal kodes mere robust.
  // Fordi jeg gerne vil returnere alle husnumre når vejen er skrevet,
  // så virker det quick and dirty hvis jeg "lader som om" vejnummeret ikke er skrevet fuldt ud.
  var q = vejnavn.substr(0, vejnavn.length-1);

  for (var i = vejnavn.length -1, length = query.length; i < length; i++) {
    q += query[i];

    var matches = matchHouses(vejnavn, previousmatches, q);
    result = Object.keys(matches);

    if (result.length === 1) {
      if (result[0].length <= q.length) {
        var data = matches[result[0]][0];
        loadFloors(postnr, data.vejnavn, data.husnr, query, promise);
        returnresult = false;
      }
    }
  }

  if (returnresult) {
    if (result.length) {
      resolveResult(promise, matches);
    }
    else {
      resolveResult(promise, previousmatches);
    }
  }
}

// Finds all floors in the house - if any.
function loadFloors(postnr, vejnavn, husnr, query, promise) {
  loadData(postnr, vejnavn, husnr).done(function(data) {
    var matches = matchFloors(vejnavn, husnr, data, query);
    var result = Object.keys(matches);
    if (result.length === 1) {
      // Ring til John.
      matches[result[0]][0].match = true;
      resolveResult(promise, matches);
    }
    else {
      resolveResult(promise, matches);
    }
  }).fail(function(error) {
    promise.reject(error);
  });
}

// Loads data from backend, or memory if we already performed a request once.
// TODO: Handle requests in progress (to avoid hammering on a cribbled backend)
function loadData(postcode, street, house) {
  var request = $.Deferred();
  var cacheKey = postcode + '__' + street + '__' + house;
  if (caching[cacheKey]) {
    request.resolve(caching[cacheKey]);
  }
  else {
    config.source(postcode, street, house)
    .done(function(data) {
      caching[cacheKey] = data;
      request.resolve(data);
    })
    .fail(function(xhr) {
      request.reject({ajax: xhr});
    });
  }
  return request.promise();
}

function resolveResult(promise, matches) {
  var result = [];
  for (var i in matches) {
    result.push(matches[i][0]);
  }
  promise.resolve(
    result.sort(function(a,b) {
      return (a.vejnavn < b.vejnavn) ? -1 : (a.husnr - b.husnr);
    })
  );
}

function loadSuggestions(postnr, query) {
  var promise = $.Deferred();

  // assertions
  if (!postnr) {
    // TODO: Decide on proper error format.
    promise.reject({validation: 'postcode:missing'});
  }
  else if (!query) {
    // TODO: Decide on proper error format.
    promise.reject({validation: 'address:missing'});
  }

  // Input ok.
  else {
    loadStreets(postnr, query, promise);
  }

  return promise.promise();
}

var visitation = {
  setup: setup, // Reset input
  loadSuggestions: loadSuggestions, // Returns JQuery promise. Promise will fail if postcode is not set

  // TODO: Remove these methods?
  isPostcodeValid: function() {
    return (postnr && postnr.toString().length === 4);
  }, // Returns bool
  isAddressValid: function() {

  } // Returns bool, Returns true, if single AMS-id is found.
};

module.exports = visitation;
