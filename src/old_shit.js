// TODOO: Fix that s*it!!


var postnr = 2770;

var _streets = {
  Nordmarksvej: [{
    display: 'Nordhavns 14 1 tv',
    data: 14
  }],
  Nordhavns: []
};
var _houses = {};

var selected

function loadAddresses(data) {
  if (data.husnumre) {
    for (var i = 0, length = data.husnumre.length; i < length; i++) {
      var street = data.husnumre[i].vejnavn;
      if (!_streets[street]) {
        _streets[street] = [];
      }
      _streets[street].push({
        display: data.husnumre[i].vejnavn + ' ' + data.husnumre[i].husnr,
        data: data.husnumre[i]
      });
    }
  }
}

function loadHouses(data) {
  for (var i = 0, length = data.length; i < length; i++) {
    var house = data[i];
    if (!_houses[house.vejnavn]) {
      _houses[house.vejnavn] = {};
      _houses[house.vejnavn][house.husnr] = [];
    }
    _houses[house.vejnavn][house.husnr].push({
      display: house.vejnavn + ' ' + house.husnr + ' ' + ((house.door || house.floor) || ''),
      data: house
    });
  }
}

function matchStreet(query, done) {
    var result = {};
    for (var street in _streets) {
      console.log(query, street, query.toLowerCase().indexOf(street.toLowerCase()));
      if (street.toLowerCase().indexOf(query.toLowerCase()) === 0
        || query.toLowerCase().indexOf(street.toLowerCase()) === 0) {
        result[street] = 1;
      }
    }

    console.log('matchStreet', Object.keys(result));
    done(Object.keys(result));
}

function matchStreetAndHouse(query, street, done) {
  var result = {};
  var houses = _streets[street];
  for (var i = 0, length = houses.length; i < length; i++) {
    var address = houses[i].display;
    if (address.toLowerCase().indexOf(query.toLowerCase()) === 0) {
      result[address] = 1;
    }
  }

  console.log('matchStreetAndHouse', Object.keys(result));
  done(Object.keys(result));
}

function matchStreetAndHouseAndFloor(query, street, housenumber, done) {
  httpGet(postnr, street, housenumber)
  .then(function (data) {
    loadHouses(data);

    var result = {};
    var floors = addresses[street][housenumber];

    for (var i = 0, length = houses.length; i < length; i++) {
      var address = houses[i].vejnavn + ' ' + houses[i].husnr;
      if (address.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        result[streetname] = 1;
      }
    }

    console.log('matchStreetAndHouseAndFloor', Object.keys(result));
    done(Object.keys(result));
  });
}

function match(query, done) {
  matchStreet(query, function (streets) {
    if (streets.length === 1) {
      matchStreetAndHouse(query, streets[0], function(houses) {
        if (houses.length === 1) {
          matchStreetAndHouseAndFloor(query, streets[0], houses[0], function(floors) {
            if (floors.length === 1) {
              // WOOHOOO!!! We found the address
              // Now what???
              console.log('WOOHOOO', floors);
              return true;
            }
            else {
              done(floors);
            }
          });
        }
        else {
          done(houses);
        }
      });
    }
    else {
      done(streets);
    }
  });
}

function stripCharacters(str, chr) {
  var result = str;
  for (var i = 0, length = chr.length; i < length; i++) {
    result = result.replace(chr, '');
  }
  return result;
}

var request;
function httpGet(postcode, street, housenumber, done) {
  if (request && request.readyState != 4) {
    request.abort();
  }
  var url = 'http://yousee.dk/kasia/adresse/find';
  if (postcode) {
    url += '/' + encodeURIComponent(postcode);
    if (street) {
      url += '/' + encodeURIComponent(street);
      if (housenumber) {
        url += '/' + encodeURIComponent(housenumber);
      }
    }

    console.log('getJSON', url);
    request = $.getJSON(url);
    return request;
  }
}


function unique(arr) {
  var result = {};
  for (var i = 0, length = arr.length; i < length; i++) {
    result[arr[i]] = 1;
  }
  return Object.keys(arr);
}

var hasData = false;
var streetTypeaheadDataset = {
  source: function (query, sync, async) {
    query = stripCharacters(query, ',.');

    if (!hasData) {
      httpGet(postnr, query)
      .then(function (data) {
        loadAddresses(data);
        match(query, async);
        hasData = true;
      });
    }
    else {
      match(query, sync);
    }
  }
}

var options = {
  highlight: true,
  autoselect: true
};

$('#address').typeahead(null,
streetTypeaheadDataset);


