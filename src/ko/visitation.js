var ko = require ('knockout');

function TextInput(options) {
  var model = {
    hasFocus: ko.observable(),
    style: ko.observable(),
    value: ko.observable(),
    data: ko.observable(),
    validation: ko.observable()
  };

  model.validate = function(validation) {
    // reset style from previous validation
    model.style({});

    // validate
    model.validation(undefined);
    validation(model.value(), function(message) {
      model.validation(message);
      model.style({borderColor:'red'});
      model.hasFocus(true);
    });
  }

  return model;
}

function InfoText() {
  var model = {
    isVisible: ko.observable()
  };

  model.toggleVisible = function() {
    model.isVisible(!model.isVisible());
  };

  return model;
}

function Visitation(options) {
  var model = {
    isMobile: ko.observable(),
    ajaxError: ko.observable(),
    ajaxLoading: ko.observable(),
    step: ko.observable(1),
    infoText: new InfoText(),
    postcode: new TextInput('postcode'),
    address: new TextInput('address'),
    customerRelation: ko.observable()
  };

  // Defaults
  options = options || {};
  options.product = options.product || 'TV'; // TV / BB
  options.errorMessages = options.errorMessages || {};
  options.errorMessages['postcode:missing'] = 'Indtast postnr.';
  options.errorMessages['postcode:invalid'] = 'Ukendt postnr.';
  options.errorMessages['address:missing']  = 'Indtast adresse';
  options.errorMessages['address:invalid']  = 'Ukendt adresse';
  options.errorMessages['address:ambiguous']  = 'Adresse er ikke unik. Vælg fra liste';
  options.errorMessages['ajax:failed']  = 'Der er ingen forbindelse til serveren';
  options.errorMessages['ajax:timeout']  = 'Der er ingen forbindelse til serveren, prøv igen senere';

  // TODO: Assert that callbacks are available.
  var loadPostcode = options.loadPostcode;
  var addressSearch = options.addressSearch;

  model.validation = ko.computed(function() {
    var postcodeError = model.postcode.validation();
    if (postcodeError) {
      return postcodeError;
    }

    var addressError = model.address.validation();
    if (addressError) {
      return addressError;
    }

    var ajaxError = model.ajaxError();
    if (ajaxError) {
      return ajaxError;
    }
  })

  function validatePostcode(value, cb) {
      // Check if input is already validated.
      if (model.postcode.data() && model.postcode.data().text == value) {
          if (cb) {cb(true); }
        return;
      }

    // Else, start validating
    var postcodeMatch = /^([1-9][0-9]{3})/.exec(value);
    if (postcodeMatch) {
      model.postcode.data(undefined);
      model.ajaxError(undefined);
      model.postcode.validation(undefined);
      return loadPostcode(postcodeMatch[1])
      .then(function (data) {
        if (data.nr) {
          var text = data.nr + ' ' + data.navn;
          model.postcode.data({
            postcode: data.nr,
            text: text
          });

          if (value != text) {
            setTimeout(function() {
              model.postcode.value(text);
            }, 1);
          }

          // Automatically advance to step2 on mobile.
          if (model.isMobile()) {
            gotoStep2();
          }
          // Automatically move cursor to address on desktop.
          else {
            model.address.hasFocus(true);
          }
          if (cb) {cb(true); }
        }
      })
      .fail(function() {
        model.ajaxError('ajax:failed');
        if (cb) {cb(false); }
      });
    }
    else if (cb) {
      cb(false);
      model.postcode.data(undefined);
      model.postcode.validate(function(value, fail) {
        return fail('postcode:missing');
      });
    }
  }

  model.postcode.value.subscribe(validatePostcode);

  function gotoStep1() {
    model.step(1);
    model.postcode.value(undefined);
    model.postcode.hasFocus(true);
    model.address.value(undefined);
  }

  function gotoStep2() {
    // Validate
    if (!model.postcode.value()) {
      model.postcode.validate(function(value, fail) {
        return fail('postcode:missing');
      });
    }
    else {
      model.step(2);
      model.address.hasFocus(true);
    }
  }

  function validateAddress(query, cb) {
    // If input is already validated.
    if (model.address.data() && model.address.data().match && model.address.data().text === query) {
          if (cb) {cb(true);}
      return;
    }

    // TODO: validate Postcode
validatePostcode(model.postcode.value(), function(success) {
  if (success) {
    model.address.data(undefined);
    model.address.validation(undefined);
    model.ajaxError(undefined);
    if (query) {
      var postcode = model.postcode.data().postcode;
      return addressSearch.loadSuggestions(postcode, query)
      .then(function (list) {
        if (list.length === 1 && list[0].match) {
          model.address.data(list[0]);
          if (cb) {cb(true);}
        }
        else if (list.length > 1 && cb) {
          model.address.validation('address:ambiguous');
          if (cb) {cb(false);}
        }
      }).fail(function(message) {
        model.ajaxError('ajax:failed');
          if (cb) {cb(false);}
      });
    }
    else if (cb) {
      cb(false);
      model.address.data(undefined);
      model.address.validate(function(value, fail) {
        return fail('address:missing');
      });
    }
  }
})
  }

  var visitationResult = ko.observable();
  model.result = ko.computed(function() {
    var result = visitationResult();
    var address = model.address.data();
    var customerRelation = model.customerRelation();
    if (result
    && address
    && customerRelation) {
      options.onVisitationComplete(address, result, customerRelation);
      return {
        result: result,
        address: address,
        customerRelation: customerRelation
      };
    }
  });

  model.isChecking = ko.observable();
  function checkAddress(product, address) {
    model.ajaxLoading(true);
    options.loadVisitationByAddress(product, address)
    .then(function(result) {
      if (loadingMessageTimeout) {
        clearTimeout(loadingMessageTimeout);
      }
      model.step3.randomLoadingMessage('Viderestiller til produktside');
      visitationResult(result);
    })
    .fail(function(xhr) {
      // TODO: Handle ODiN Api fail
      model.ajaxError('ajax:failed');
    }).
    always(function() {
      model.ajaxLoading(false);
    });
  }

  model.address.value.subscribe(validateAddress);

  function gotoStep3() {
    if (model.address.data()) {
      model.step(3);
      checkAddress(options.product, model.address.data());
    }
    else {
      validateAddress(model.address.value(), function(success) {
        if (success) {
          model.step(3);
          checkAddress(options.product, model.address.data());
        }
      });
    }
  }

  model.step1 = {
    hasFocus: ko.observable(),
    button: {
      click: gotoStep2
    }
  };

  model.step1.button.enabled = ko.computed(function() {
    var postcodeInput = model.postcode.value();
    var postcodeData = model.postcode.data();
    return (postcodeInput === undefined || postcodeInput === '') || (postcodeInput.length && postcodeData);
  });

  model.step2 = {
    backButton: {
      click: gotoStep1
    },
    submitButton: {
      click: gotoStep3
    }
  };

  model.step1.hasFocus = ko.computed(function() {
    if (model.isMobile()) {
      return model.postcode.hasFocus();
    }
  }),

  model.step2.hasFocus = ko.computed(function() {
    if (model.isMobile()) {
      return model.address.hasFocus();
    }
  }),

  model.step2.postcode = ko.computed(function() {
    if (model.postcode.data()) {
      return model.postcode.data().text;
    }
  });

  // Step 3
  function submitCustomerRelation() {
    if (model.customerRelation()) {
      model.step3.isFormVisible(false);
      showRandomLoadingMessage();
    }
  }

  var loadingMessageTimeout;
  var loadingMessageIndex = 0;
  function showRandomLoadingMessage(item) {
    if (!item) {
      item = options.loadingMessages[loadingMessageIndex++ % options.loadingMessages.length];
    }
    model.step3.randomLoadingMessage(item.Body); // TODO: BindingHandler fadein
    if (item.SecondsToDisplay) {
      loadingMessageTimeout = setTimeout(showRandomLoadingMessage, item.SecondsToDisplay*1000);
    }
  }

  model.step3 = {
    isFormVisible: ko.observable(true),
    randomLoadingMessage: ko.observable(),
    button: {
      click: submitCustomerRelation
    }
  };

  model.step3.isLoadingVisible = ko.computed(function() {
    return model.ajaxLoading() && !model.step3.isFormVisible();
  }),

  model.step2.submitButton.enabled = ko.computed(function() {
    return true;
  });

  model.step3.button.enabled = ko.computed(function() {
    return !!model.customerRelation();
  });

  // TODO: setup typeahead
    var typeaheadOptions = {
        highlight: true,
        autoselect: true,
        hint: false
    }

    model.postcode.typeahead = {
        options: typeaheadOptions,
        observable: model.postcode.data,
        dataset: {
            async: true,
            source: function (query, render) {
                $.getJSON('https://dawa.aws.dk/postnumre/autocomplete', { 'q': query })
                .then(function (data) {
                    var suggestions = data.filter(function (item) {
                        return item.postnummer;
                    }).map(function (item) {
                        return { data: item.postnummer, value: item.tekst };
                    });
                    render(suggestions);
                })
                .fail(function() {
                    // TODO: Handle dawa fail
                });
            },
            templates: {
                suggestion: function (data) {
                    return data.value;
                }
            },
            limit: 5
        }
    };

    model.address.typeahead = {
        options: typeaheadOptions,
        observable: model.address.data,
        dataset: {
            async: true,
            source: function (query, render) {
                var postcode = model.postcode.value().substr(0, 4);
                addressSearch.loadSuggestions(postcode, query)
                .then(function (list) {
                    var suggestions = list
                    .map(function (item) {
                        return { data: item, value: item.text };
                    });
                    render(suggestions);
                })
            },
            templates: {
                suggestion: function (data) {
                    return data.value;
                }
            },
            limit: 5
        }
    };

  model.desktop = {
    postcode: model.postcode,
    address: model.address,
    validation: model.validation,
    button: {
      click: function() {
            validateAddress(model.address.value(), function(success) {
              if (success) {
                gotoStep3();
              }
            });
      }
    }
  };

  model.desktop.hasFocus = ko.computed(function() {
    if (model.isMobile()) {
      return;
    }
    if (model.postcode.hasFocus()) {
      return 'postcode';
    }
    if (model.address.hasFocus()) {
      return 'address';
    }
  })

  model.desktop.button.enabled = ko.computed(function() {
    if (!model.postcode.value()) {
      return true;
    }
    if (model.postcode.data()) {
      return true;
    }
  });

  return model;
}

module.exports = Visitation;
