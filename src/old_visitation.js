var config = {};
var state = {};

function setup(settings) {
  $.extend(config, settings);
  state = {
    postCode: null,
    level: 0,
    adresses: [],
    query: '',
    data: {},
    streets: {},
    houses: {},
    streetName: ''
  };
}

function setState(key, value) {
  if (state.hasOwnProperty(key)) {
    state[key] = value;
  }
}

function setPostcode(postCode) {
  postnr = postCode;
  setState('postCode', postCode);
}

function searchArr(needle, haystack) {
  //console.log(needle);
  var matches = [];
  var lastFoundItem = '';
  for (var i = 0; i < haystack.length; i++) {
    if (needle.search(haystack[i]) === 0 || haystack[i].search(needle) === 0) {
      lastFoundItem = haystack[i];
      matches.push(lastFoundItem);
    }
  }
  //console.log()
  return matches;
  /*
  if (count === 1) {
    setState('adresses', [lastFoundItem]);
    setState('streetName', lastFoundItem);
    console.log(lastFoundItem);
    return true;
  } else {
    setState('adresses', matches);
    return false;
  }
  */
}

function matchStreets(data, query) {
  if (data.husnumre) {
    for (var i = 0, length = data.husnumre.length; i < length; i++) {
      var street = data.husnumre[i].vejnavn;
      if (!state.streets[street]) {
        state.streets[street] = [];
      }
      state.streets[street].push({
        display: data.husnumre[i].vejnavn + ' ' + data.husnumre[i].husnr,
        data: data.husnumre[i]
      });
    }
    return searchArr(query, Object.keys(state.streets));
  }
}

function matchHouses() {
  var houses = [];
  for (var i = 0; i < state.streets[state.streetName].length; i++) {
    houses.push(state.streets[state.streetName][i].display);
  }
  return houses;
}

function match(query) {
  var promise = $.Deferred();
  if (!state.level) {
    state.level = 1;
    return match(query);
  }

  // Hvad skal der ske når vi er i level 1
  else if (state.level === 1) {

    loadData(state.postCode, query).done(function(data) {
      var matches = matchStreets(data, query);
      // console.log(matches);
      if (matches.length === 1) {
        state.match = matches[0].display;
        state.level = 2;
        //return match(query);
      }
      else {
        promise.resolve({
          level: state.level,
          matches: matches
        });
      }
    });
  }

  // Hvad skal der ske når vi er i level 2
  else if (state.level === 2) {
    if (state.query.indexOf(state.match) === -1) {
      state.level = 1;
      return match(query);
    }
    else {
      var matches = matchHouses();
      if (matches.length === 1) {
        state.match = matches[0];
        state.level = 3;
        return match(query);
      }
      else {
        promise.resolve({
          level: state.level,
          matches: matches
        });
      }
    }
  }

  // Hvad skal der ske når vi er i level 2
  else if (state.level === 3) {
    if (state.query.indexOf(state.match) === -1) {
      state.level = 2;
      return match(query);
    }
    else {
      var floors = matchFloors();
      if (floors.length === 1) {
        state.match = floors[0].display;
        state.level = 4;
      }
    }
  }
  return promise.promise();
}


function matchLevel1(postnr, query, promise) {
  loadData(postnr, query).done(function(data) {
    var matches = matchStreets(data, query);
    var result = Object.keys(matches);
    if (result.length === 1) {
      state.match = result[0];
      matchLevel2(result[0].data.vejnavn, query, promise);
    }
    else {
      promise.resolve(result);
    }
  });
}

function matchLevel2(streetname, query, promise) {
  loadData(state.postCode, query).done(function(data) {
    var matches = matchHouses(streetname, data, query);
    var result = Object.keys(matches);
    if (result.length === 1) {
      state.match = result[0];
      matchLevel2(result[0].data.vejnavn, query, promise);
    }
    else {
      promise.resolve(result);
    }
  });
}


var postnr;
var vejnavn;
var husnr;

function match(query, promise, level) {
  switch(level) {
    case 1:
      matchLevel1(query, promise);
    break;
    default:
      match(query, promise, 1);
  }
}

function loadData(postcode, street, house) {
  var request = $.Deferred();
  var cacheKey = postcode + '__' + street;
  if (state.data[cacheKey]) {
    request.resolve(state.data[cacheKey]);
  }
  else {
    config.source(postcode, street, house).done(function(data) {
      state.data[cacheKey] = data;
      request.resolve(data);
    })
  }
  return request.promise();
}

function loadSuggestions(query) {
  var promise = $.Deferred();
  match(query, promise);
  return promise.promise();
}

var visitation = {
  // TODO: Figure out module signature
  setup: setup, // Reset input
  setPostcode: setPostcode,
  setState: setState,
  // filterSuggestions: filterSuggestions,
  loadSuggestions: loadSuggestions, // Returns JQuery promise. Promise will fail if postcode is not set
  isPostcodeValid: function() {}, // Returns bool
  isAddressValid: function() {} // Returns bool, Returns true, if single AMS-id is found.
};

module.exports = visitation;
