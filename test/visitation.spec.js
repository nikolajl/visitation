var visitation = require('../src/visitation');
window.$ = require('jquery');

function call_endpoint(postcode, street, house) {
  if (postcode) {
    if (street) {
      if (house) {
        return $.getJSON('http://yousee.dk/kasia/adresse/find/' + postcode + '/' + street + '/' + house);
      }
      return $.getJSON('http://yousee.dk/kasia/adresse/find/' + postcode + '/' + street);
    }
    return $.getJSON('http://yousee.dk/kasia/adresse/find/' + postcode);
  }
}

function mockJson(data) {
  var mock = $.Deferred();
  mock.resolve(data);
  return mock;
}

function mockFindAddress(postcode, street, house) {
  if (postcode) {
    if (street) {
      if (house) {
        return mockJson(require('json!./data/kasia/adresse/find/2770__Nordmarksvej__3.json'));
      }
      return mockJson(require('json!./data/kasia/adresse/find/2770__Nord.json'));
    }
    return mockJson(require('json!./data/kasia/adresse/find/2770__Nord.json'));
  }
  return mockJson(require('json!./data/kasia/adresse/find/nodata.json'));
}

function generateAddress(data) {
  return data.vejnavn + ' ' + data.husnr
    + (data.etage ? ' ' + data.etage : '')
    + (data.doernr ? ' ' + data.doernr : '');
}

function addressToString(address) {
  if (address.match) {
    return 'Ring til John';
  }

  return address.vejnavn
  + (address.husnr ? ' ' + address.husnr : '')
  + (address.etage ? ' ' + address.etage : '')
  + (address.doernr ? ' ' + address.doernr : '');
}

describe('Visitation', function() {
  describe('loadSuggestions', function() {
    xit('should return from ajax', function(testCompleted) {
      visitation.setup({
        source: call_endpoint
      });

      var ajax = visitation.loadSuggestions('2770', 'N')
      .done(function(data) {
        // Assumes data is an array (something with property length)
        expect(data.length).toBeDefined();
        testCompleted();
      });
    });

    it('should fail if postcode is not set', function(testCompleted) {
      visitation.setup({
        source: function() {
          fail('source should not be called');
        }
      });

      var promise = visitation.loadSuggestions('', 'Nord');
      promise.done(function(data) {
        fail('result should not be returned');
      });

      promise.fail(function() {
        expect(true).toBe(true);
      });

      promise.always(function() {
        testCompleted();
      });
    });

    it('should fail if postcode is set, but query is undefined/null or empty string)', function(testCompleted) {
      visitation.setup({
        source: function() {
          fail('source should not be called');
        }
      });

      var promise = visitation.loadSuggestions('2770', '');
      promise.done(function(data) {
        fail('result should not be returned');
      });

      promise.fail(function() {
        expect(true).toBe(true);
      });

      promise.always(function() {
        testCompleted();
      });
    });

    it('should call source when fetching remote data', function(testCompleted) {
      var config = {
        source: mockFindAddress
      };
      spyOn(config, 'source').and.callThrough();
      visitation.setup(config);

      visitation.loadSuggestions('2770', 'Nord').done(function(data) {
        expect(config.source).toHaveBeenCalled();
        testCompleted();
      });
    });

    it('should return 3 results, given postcode "2770" and query "Nord"', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nord').done(function(data) {
        expect(data.length).toBe(3);
        testCompleted();
      });
    });

    it('should return 1 result; "Nordmarksvej", given postcode "2770" and query "Nordmarksve"', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksve').done(function(data) {
        expect(data.length).toBe(1);
        //expect(data).toContain(['Nordmarksvej']);
        testCompleted();
      });
    });

    it('should return 65 adresses, given postcode "2770" and query "Nordmarksvej"', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej').done(function(data) {
        expect(data.length).toBe(65);
        testCompleted();
      });
    });

    it('should return 19 addresses, given postcode "2770" and query "Nordmarksvej 1"', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej 1').done(function(data) {
        //console.log(data.map(addressToString));
        expect(data.length).toBe(19);
        testCompleted();
      });
    });

    it('should return 4 addresses, given postcode "2770" and query "Nordmarksvej 10', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej 10').done(function(data) {
        expect(data.length).toBe(4);
        testCompleted();
      });
    });

    it('should return 6 addresses, given postcode "2770" and query "Nordmarksvej 38', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej 38').done(function(data) {
        expect(data.length).toBe(6);
        testCompleted();
      });
    });

    it('should return 2 results, given postcode "2770" and query "Nordmarksvej 38 ST', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej 38 ST TH').done(function(data) {
        expect(data.length).toBe(1);
        testCompleted();
      });
    });

    it('should return 1 result: "Ring til John" , given postcode "2770" and query "Nordmarksvej 38 ST TH"', function(testCompleted) {
      visitation.setup({
        source: mockFindAddress
      });

      visitation.loadSuggestions('2770', 'Nordmarksvej 38 ST TH').done(function(data) {
        expect(data.length).toBe(1);
        testCompleted();
      });
    });

    // // TODO: Rename this to something more intelligent ?
    function testMultipleSingleAddresses(address, completeDataSet, dataSet) {
      var generatedAddress = generateAddress(address);
      it('should return 1 result: "Ring til John" , given postcode "2770" and query "' + generatedAddress + '"', function(testCompleted) {
        visitation.setup({
          source: function(postcode, street, house) {
            if (postcode) {
              if (street) {
                if (house) {
                  return mockJson(completeDataSet);
                }
                return mockJson(dataSet);
              }
              return mockJson(dataSet);
            }
            return mockJson(require('json!./data/kasia/adresse/find/nodata.json'));
          }
        });

        visitation.loadSuggestions(address.postnr, generatedAddress).done(function(data) {
          expect(data.length).toBe(1);
          testCompleted();
        });
      });
    }

    var completeDataSet = require('json!./data/kasia/adresse/find/2770__Nordmarksvej__3.json');
    var dataSet = require('json!./data/kasia/adresse/find/2770__Nord.json');
    for (var i = 0; i < completeDataSet.length; i++) {
      testMultipleSingleAddresses(completeDataSet[i], completeDataSet, dataSet);
    }

    it('TODO: should fail when backend returns error');

    xit('should return results for "2400, Tomsgårdsvej 13B 4 TV', function(testCompleted) {

      visitation.setup({
        source: call_endpoint
      });

      visitation.loadSuggestions('2400', 'Tomsgårdsvej 13B 4').done(function(data) {
        expect(data.length).toBe(2);
        testCompleted();
      });
    });

  });
});
