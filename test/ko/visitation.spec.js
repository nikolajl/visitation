var Visitation = require('../../src/ko/visitation');
//window.$ = require('jquery');
//window.ko = require('knockout');
require('jquery');

function asyncTest(test, delay) {
  delay = delay || 10;
  return function(done) {
    setTimeout(function() {
      test();
      done();
    }, delay);
  }
}

function Promise(data, delay) {
  var request = $.Deferred();
  if (delay) {
    setTimeout(function() {
      request.resolve(data);
    }, delay);
  }
  else {
    request.resolve(data);
  }
  return request.promise();
}

function loadPostcode(postcode) {
  if (postcode.length < 4) {
    return Promise({});
  }
  return Promise({nr: '9000', navn: 'Aalborg'});
  //$.getJSON('https://dawa.aws.dk/postnumre/' + postcodeMatch[1])
}

function emptySuggestion() {
  return Promise([]);
}

function matchingSuggestion() {
  return Promise([{
    vejnavn: 'Herluf Trolles Gade',
    doernr: 'TV',
    postnr: '9000',
    bynavn: 'Aalborg',
    kommune: 'Aalborg',
    husnr: '34',
    amsid: '2595697',
    etage: '1',
    match: true,
    text: 'Herluf Trolles Gade 34 1 TV'
  }]);
}

function visitationCoax() {
  return Promise({
    amsid: null,
    destinationUrl: 'https://tdc.dk/cobberBB',
    technology: ['Cobber', 'CobberTV', 'CoaxAvailableOnAddress']
  }, 100);
}

var options = {
  loadPostcode: loadPostcode,
  addressSearch: {
    loadSuggestions: emptySuggestion
  },
  loadVisitationByAddress: visitationCoax
};

var viewmodel = new Visitation(options);

fdescribe('Visitation viewmodel', function() {
  describe('Initial state', function() {
    it('should have read more hidden', function() {
      expect(viewmodel.infoText.isVisible()).toBeFalsy();
    });

    it('should show read more when clicking the link', function() {
      expect(viewmodel.infoText.isVisible()).toBeFalsy();
      viewmodel.infoText.toggleVisible();
      expect(viewmodel.infoText.isVisible()).toBeTruthy();
      viewmodel.infoText.toggleVisible();
      expect(viewmodel.infoText.isVisible()).toBeFalsy();
    });

    it('should have the "Next"-button enabled', function() {
      expect(viewmodel.step1.button.enabled()).toBeTruthy();
    });

    it('should not have postcode input focused', function() {
      expect(viewmodel.postcode.hasFocus()).toBeFalsy();
    });
  });

  describe('Step1: (mobile)', function() {
    beforeEach(function() {
      viewmodel.isMobile(true);
    });
    xdescribe('when focus on the postcode input', function() {
      it('should scroll the page to the postcode input', function() {
        viewmodel.postcode.hasFocus(true);
        expect(viewmodel.postcode.scrollToThis()).toBeTruthy();
      });
    });

    it('should test something about isMobile()');

    describe('when selecting the "Next"-button while postcode input is empty', function() {
      beforeEach(function() {
        viewmodel.postcode.value('');
        viewmodel.step1.button.click();
      });

      it('should show an error message', asyncTest(function() {
        expect(viewmodel.validation()).toBeTruthy();
      }));

      it('should show a red border on the postcode input', asyncTest(function() {
        expect(viewmodel.postcode.style()).toBeDefined();
        expect(viewmodel.postcode.style().hasOwnProperty('borderColor')).toBeTruthy();
      }));

      it('should set focus on the postcode input', asyncTest(function() {
        expect(viewmodel.postcode.hasFocus()).toBe(true);
      }));
    });

    describe('when input contains invalid postcode', function() {
      beforeEach(function() {
        viewmodel.postcode.value('0');
      });

      it('should disable the "Next"-button', asyncTest(function() {
        expect(viewmodel.step1.button.enabled()).toBeFalsy();
      }));
    });

    describe('when input contains valid postcode', function() {
      beforeEach(function() {
        viewmodel.postcode.value('9000');
      });

      it('should enable the "Next"-button', asyncTest(function() {
        expect(viewmodel.step1.button.enabled()).toBeTruthy();
      }));

      it('should automatically proceed to step 2', asyncTest(function() {
        expect(viewmodel.step()).toBe(2);
      }));

      describe('and selecting the "Next"-button (in case, the browser did not manage to automatically advance)', function() {
        it('should proceed to step 2', asyncTest(function() {
          expect(viewmodel.step()).toBe(2);
        }));
      });
    });
  });

  describe('Step 2: (mobile)', function() {
    beforeEach(function() {
      viewmodel = new Visitation(options);
      viewmodel.isMobile(true);

      // TODO: set step(2) / postcode.data(...)
      viewmodel.postcode.value('9000');
    });

    it('should show postcode + city from step 1', asyncTest(function() {
      expect(viewmodel.postcode.value()).toBe('9000 Aalborg');
    }));

    it('should have adress input focused', asyncTest(function() {
      expect(viewmodel.address.hasFocus()).toBeTruthy();
    }));

    describe('when going back to step 1 (to edit postcode)', function() {
      it('should clear the postcode input', asyncTest(function() {
        viewmodel.step2.backButton.click();
        expect(viewmodel.postcode.value()).toBe(undefined);
      }));

      it('should clear the address input', asyncTest(function() {
        viewmodel.step2.backButton.click();
        expect(viewmodel.address.value()).toBe(undefined);
      }));

      it('should focus the postcode input', asyncTest(function() {
        viewmodel.step2.backButton.click();
        expect(viewmodel.postcode.hasFocus()).toBeTruthy();
      }));
    });

    describe('when address input is empty and selecting the "Submit"-button', function() {
      beforeEach(function() {
        viewmodel.address.value('');
        viewmodel.step2.submitButton.click();
      });

      it('should show the postcode:missing error', function() {
        expect(viewmodel.validation()).toBe(options.errorMessages['postcode:missing']);
      });
    });

    describe('when address input is invalid', function() {
      beforeEach(function() {
        options.addressSearch.loadSuggestions = emptySuggestion;
        viewmodel = new Visitation(options);
        viewmodel.postcode.value('9000');
        viewmodel.address.value('fdjskl');
      });

      it('should enable the "Submit"-button', asyncTest(function() {
        expect(viewmodel.step2.submitButton.enabled()).toBeTruthy();
      }));
    });

    describe('when address input is valid', function() {
      beforeEach(function() {
        options.addressSearch.loadSuggestions = matchingSuggestion;
        viewmodel = new Visitation(options);
        viewmodel.postcode.value('9000');
        viewmodel.address.value('Herluf Trolles Gade 34 1 TV');
      });

      it('should enable the "Submit"-button', asyncTest(function() {
        expect(viewmodel.step2.submitButton.enabled()).toBeTruthy();
      }));

      describe('and selecting the "Submit"-button', function() {
        beforeEach(function() {
          viewmodel.step2.submitButton.click();
        });

        it('should disable the "Submit"-button (so the user cannot hit it twice)', function() {
          expect(viewmodel.step2.submitButton.enabled()).toBeFalsy();
        });

        it('should proceed to step 3', asyncTest(function() {
          expect(viewmodel.step()).toBe(3);
        }, 10));

        it('should start checking address', asyncTest(function() {
          expect(viewmodel.step3.isLoading()).toBeTruthy();
        }, 10));

        it('should show customer identification form', asyncTest(function() {
          expect(viewmodel.step3.isFormVisible()).toBeTruthy();
        }, 10));
      });
    });
  });


  describe('Step 3:', function() {
    beforeEach(function() {
      options.addressSearch.loadSuggestions = matchingSuggestion;
      options.onVisitationComplete = function() {};
      viewmodel = new Visitation(options);
      viewmodel.postcode.value('9000 Aalborg');
      viewmodel.address.value('Herluf Trolles Gade 34 1 TV');
      viewmodel.step2.submitButton.click();

      spyOn(options, 'onVisitationComplete');
    });

    describe('when selecting customer type', function() {
      beforeEach(function() {
        viewmodel.customerRelation('something');
      });

      it('should enable "Submit"-button', asyncTest(function() {
        expect(viewmodel.step3.button.enabled()).toBeTruthy();
      }, 10));

      describe('and hitting "Submit"-button', function() {
        beforeEach(function() {
          viewmodel.step3.button.click();
        });

        describe('when visitation check is still loading', function() {
          it('should show loading screen', asyncTest(function() {
            expect(viewmodel.step3.isFormVisible()).toBeFalsy();
            expect(viewmodel.step3.isLoading()).toBeTruthy();
          }, 10));
        });

        describe('when visitation check complete', function() {
          it('should redirect to the correct url', asyncTest(function() {
            expect(options.onVisitationComplete).toHaveBeenCalled();
          }, 200));
        });
      });
    });

  });
});

/*
'http://teamtakecare.local.internal.tdc.dk/api/p/visitation/visitationbyaddress'
{
 "amsid": null,
 "destinationUrl": "https://tdc.dk/cobberBB",
 "technology": [
   "Cobber",
   "CobberTV",
   "CoaxAvailableOnAddress"
 ]
}
*/
