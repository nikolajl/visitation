# YouSee Visitation Engine

1. Clone the repository
2. Install the Karma Runner globally `npm install -g karma`
3. Go to the repository folder `cd visitation`
4. Install dependencies `npm install`
5. _Optionally run the test suite_ `karma start`

![alt text](https://cdn.meme.am/instances/22138679.jpg "Have all the fun!")
